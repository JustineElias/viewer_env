from PyQt5 import QtWidgets, QtCore
import vtk
import sys
import os
import vtkQtFrame
from archive.test18imagedata import make_image_from_csv_file

# TODO: Positionnement initial de la grille par rapport au plan ?
# TODO: Ajouter le plan.
# TODO: Lut ? Possible de rendre les valeurs nulles transparentes ?
# TODO: Bordures transparentes ?
# TODO: Question les données qui sortent de l'algo sont aussi des données point ?


# Call back function to resize the cone
class MyWindow:
    def __init__(self):
        self.main()

    def main(self):
        app = QtWidgets.QApplication(sys.argv)
        self.render_widget = vtkQtFrame.VtkQtFrame()

        colors = vtk.vtkNamedColors()

        # # Main pipeline
        # Create a vtkImageData
        self.img = make_image_from_csv_file()


        self.cubeSource = vtk.vtkCubeSource()
        size = 2
        self.cubeSource.SetXLength(size)
        self.cubeSource.SetYLength(size)
        self.cubeSource.SetZLength(size/5)
        # This allows to put cubes centered on the points
        self.glyph3D = vtk.vtkGlyph3D()
        self.glyph3D.SetSourceConnection(self.cubeSource.GetOutputPort())
        self.glyph3D.SetInputData(self.img)
        self.glyph3D.SetColorModeToColorByScalar()
        self.glyph3D.Update()

        self.img_mapper = vtk.vtkDataSetMapper()
        # self.img_mapper.SetInputData(self.img)
        self.img_mapper.SetInputConnection(self.glyph3D.GetOutputPort())
        self.img_actor = vtk.vtkActor()
        self.img_actor.SetMapper(self.img_mapper)
        self.render_widget.VTKRenderer.AddActor(self.img_actor)
        print(29, self.img_actor.GetProperty().GetRepresentation())
        self.img_actor.GetProperty().EdgeVisibilityOn()

        # # Display in custom vtk Qt Window
        self.window = QtWidgets.QMainWindow()
        self.window.setCentralWidget(self.render_widget)

        # Buttons
        self.clipper = None
        self.b_color_by = QtWidgets.QPushButton(self.window)
        self.b_color_by.setText("Colorer par parametre")
        self.b_color_by.clicked.connect(self.colorByParameter)

        self.plane_widget = None
        self.b_add_clipper = QtWidgets.QCheckBox(self.window)
        self.b_add_clipper.setStyleSheet("color: white")
        self.b_add_clipper.setText("Ajouter un plan de coupe")
        self.b_add_clipper.stateChanged.connect(self.add_clipping_plane)
        self.b_add_clipper.move(0, 50)

        self.render_widget.VTKRenderer.SetBackground(0, 0, 0)
        self.render_widget.VTKRenderer.ResetCamera()

        self.window.show()
        # start the event loop
        try:
            sys.exit(app.exec_())  # clean exit
        except SystemExit as e:
            if e.code != 0:
                raise ()
            os._exit(0)

    # def planeCallback(self, obj, event):
    #     self.plane.SetOrigin(self.plane_widget.GetOrigin())
    #     self.plane.SetNormal(self.plane_widget.GetNormal())

    def colorByParameter(self):

        array = self.glyph3D.GetInput().GetCellData().GetArray(49)
        self.glyph3D.GetInput().GetCellData().SetScalars(array)

        scalar_range = array.GetRange()
        print(71, type(array), scalar_range)
        values = array.GetValue(0)
        print(73)
        name = array.GetName()
        print(74, values, name)
        # self.glyph3D.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataSetAttributes.SCALARS, name)
        print(97)

        if self.clipper is not None:
            data_color_mapper = vtk.vtkDataSetMapper()
            data_color_mapper.SetInputConnection(self.clipper.GetOutputPort())
        else:
            data_color_mapper = self.img_mapper
        data_color_mapper.SelectColorArray(name)
        data_color_mapper.SetScalarRange(scalar_range)

        # Create the lookup table
        data_color_mapper.CreateDefaultLookupTable()
        lut = data_color_mapper.GetLookupTable()
        data_color_mapper.GetLookupTable().UseBelowRangeColorOn()
        data_color_mapper.GetLookupTable().SetTableRange(0.01, 1)
        data_color_mapper.GetLookupTable().SetValueRange(0, 1)
        data_color_mapper.GetLookupTable().SetAlphaRange(0, 1)
        # data_color_mapper.GetLookupTable().SetNanColor(0.2, 0.2, 0.2, 0.5)
        print(lut)

        # Create the actor
        if self.clipper is not None:
            # data_color_actor = vtk.vtkActor()
            # data_color_actor.SetMapper(data_color_mapper)
            # self.render_widget.VTKRenderer.AddActor(data_color_actor)
            self.clipper_actor.SetMapper(data_color_mapper)
        else:
            self.img_actor.SetMapper(data_color_mapper)

        print(124, self.glyph3D.GetColorModeAsString())

        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToVertical()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()

        self.render_widget.VTKRenderWindowInteractor.Initialize()

    def add_clipping_plane(self):
        if self.b_add_clipper.isChecked() and self.img_actor.GetVisibility():
            self.combobox_axis_cilpping_plane = QtWidgets.QComboBox(self.window)
            self.combobox_axis_cilpping_plane.addItems(["X axis", "Y axis", "Z Axis"])
            self.combobox_axis_cilpping_plane.move(0, 100)
            self.combobox_axis_cilpping_plane.show()
            self.combobox_axis_cilpping_plane.currentIndexChanged.connect(self.align_plan_on_axis)

            if self.plane_widget is None:
                # A plane widget
                # self.plane_widget = vtk.vtkImagePlaneWidget()
                # self.plane_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
                # self.plane_widget.SetProp3D(self.img_actor)
                # self.plane_widget.SetPlaceFactor(1.25)  # Make the box 1.25x larger than the actor
                # self.plane_widget.PlaceWidget()
                # self.plane_widget.GeneratePlane()
                self.plane_widget = 1

                bounds = self.img_mapper.GetBounds()

                # Create plane from plane_widget
                self.plane = vtk.vtkPlane()
                # self.plane = self.plane_widget.GetPlane()
                self.plane.SetOrigin(bounds[0], bounds[2], bounds[4])
                self.plane.SetNormal([1, 0, 0])

                # Create the clipped version of the data
                self.clipper = vtk.vtkClipDataSet()
                self.clipper.SetInputData(self.img)
                self.clipper.SetClipFunction(self.plane)
                self.clipper_mapper = vtk.vtkDataSetMapper()
                self.clipper_mapper.SetInputConnection(self.clipper.GetOutputPort())
                # imgdataMapper.SetInputData(self.imageData)
                self.clipper_actor = vtk.vtkActor()
                self.clipper_actor.SetMapper(self.clipper_mapper)
                # self.clipper_actor.GetProperty().EdgeVisibilityOn()
                # self.clipper_actor.GetProperty().SetColor(colors.GetColor3d("BurlyWood"))
                # self.clipper_actor.GetProperty().SetRepresentationToWireframe()
                self.render_widget.VTKRenderer.AddActor(self.clipper_actor)

                # Connect the event to a function
                # self.plane_widget.AddObserver("InteractionEvent", self.planeCallback)

            # Hide the original img actor and show the clipper actor
            self.img_actor.VisibilityOff()
            # self.plane_widget.On()
            self.clipper_actor.VisibilityOn()
        else:
            self.img_actor.VisibilityOn()
            # self.plane_widget.Off()
            self.clipper_actor.VisibilityOff()
            self.combobox_axis_cilpping_plane.hide()
            try:
                self.slider_axis_cilpping_plane.hide()
            except:
                pass

        # Following line updates the renderer so that all the action above are displayed
        self.render_widget.VTKRenderWindowInteractor.Initialize()

    def align_plan_on_axis(self):
        # normal = self.plane_widget.GetNormal()
        # self.slider_value = self.plane_widget.GetOrigin()
        # print(173, self.slider_value, normal)
        self.slider_axis_cilpping_plane = QtWidgets.QSlider(QtCore.Qt.Vertical, self.window)
        bounds = self.img_mapper.GetBounds()
        print(176, bounds)
        if self.combobox_axis_cilpping_plane.currentText() == "X axis":
            normal = [1, 0, 0]
            range_slider = [bounds[0], bounds[1]]
        elif self.combobox_axis_cilpping_plane.currentText() == "Y axis":
            normal = [0, 1, 0]
            range_slider = [bounds[2], bounds[3]]
        else:
            normal = [0, 0, -1]
            range_slider = [bounds[4], bounds[5]]

        print(187, normal, range_slider)

        self.plane.SetNormal(normal[0], normal[1], normal[2])
        self.plane.SetOrigin(bounds[0], bounds[2], bounds[4])

        self.slider_axis_cilpping_plane.setRange(range_slider[0], range_slider[1])
        self.slider_value = self.slider_axis_cilpping_plane.value()
        self.slider_axis_cilpping_plane.setFocusPolicy(QtCore.Qt.NoFocus)
        self.slider_axis_cilpping_plane.setMinimumSize(5, 200)
        self.slider_axis_cilpping_plane.setMaximumSize(5, 200)
        # self.slider_axis_cilpping_plane.setStyleSheet(
        #     "QSlider::groove:vertical {border: 1px solid; height: 10px; margin: 0px;}\n"
        #     "QSlider::handle:vertical {background-color: white; border: 1px solid; height: 40px; width: 40px; margin: -15px 0px;}")
        self.slider_axis_cilpping_plane.move(20, 150)
        self.slider_axis_cilpping_plane.valueChanged.connect(self.connect_slider_to_plane)
        self.slider_axis_cilpping_plane.show()
        # self.plane.SetOrigin(self.slider_value[0], self.slider_value[1], self.slider_value[2])
        # self.plane_widget.Update()


    def connect_slider_to_plane(self):
        self.plane.Push(self.slider_axis_cilpping_plane.value() - self.slider_value)
        print(self.plane.GetOrigin())
        self.slider_value = self.slider_axis_cilpping_plane.value()


# interactor.Start()
if __name__ == '__main__':
    w = MyWindow()
