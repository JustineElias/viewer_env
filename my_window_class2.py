from PyQt5 import QtWidgets, QtCore
import vtk
import sys
import os
import vtkQtFrame
# from cvs_reader_from_web import polyDataOutput
from from_csv_to_polydata import Reader
from my_checkbutton_and_combobox_class import MyCheckbutton



from inspect import currentframe, getframeinfo


# print(getframeinfo(currentframe()).filename, getframeinfo(currentframe()).lineno)



# from cvs_reader_from_web import polyDataOutput

class MyWindow(QtWidgets.QMainWindow):
    def __init__(self, filepath):
        super(MyWindow, self).__init__()
        dw = QtWidgets.QDesktopWidget()
        x = dw.width()
        y = dw.height()
        self.resize(x * 0.5, y * 0.7)


        self.reader = Reader(filepath)

        self.initUI()

    def initUI(self):
        self.render_widget = vtkQtFrame.VtkQtFrame()


        self.polydata = self.reader.polydata
        self.polydata_mapper = vtk.vtkDataSetMapper()
        self.polydata_mapper.SetInputData(self.polydata)

        self.make_glyph()

        self.render_widget.VTKRenderer.AddActor(self.glyph_actor)

        self.render_widget.VTKRenderer.SetBackground(0, 0, 0)

        # reset the camera and set anti-aliasing to 2x
        self.render_widget.VTKRenderer.ResetCamera()

        # add and show
        self.setCentralWidget(self.render_widget)


        list_parameters = []
        for i in range(self.polydata.GetPointData().GetNumberOfArrays()):
            list_parameters.append(self.polydata.GetPointData().GetArrayName(i))
        txt_p = "Colorer par parametre"
        self.b_color_by_pameter = MyCheckbutton(self, self.color_by_parameter, list_parameters, txt_p)


        list_planes = ["Coupe libre", "Coupe en profondeur"]
        txt_pl = "Ajouter un plan de coupe"
        self.b_add_clipper = MyCheckbutton(self, self.make_and_align_plane, list_planes, txt_pl)
        self.b_add_clipper.move(0, 50)
        self.plane_widget = None

        self.render_widget.VTKRenderWindow.Render()

    def make_glyph(self):
        self.cubeSource = vtk.vtkCubeSource()
        size = self.reader.cell_size
        print(size)
        size = [2, 2, 2]
        self.cubeSource.SetXLength(size[0])
        self.cubeSource.SetYLength(size[1])
        self.cubeSource.SetZLength(size[2] / 5)
        # This allows to put cubes centered on the points
        self.glyph3D = vtk.vtkGlyph3D()
        self.glyph3D.SetSourceConnection(self.cubeSource.GetOutputPort())
        self.glyph3D.SetInputDataObject(self.polydata)

        self.glyph3D.SetColorModeToColorByScalar()
        self.glyph3D.ScalingOff()
        self.glyph3D.Update()

        self.glyph_mapper = vtk.vtkDataSetMapper()
        # self.img_mapper.SetInputData(self.img)
        self.glyph_mapper.SetInputConnection(self.glyph3D.GetOutputPort())
        self.glyph_actor = vtk.vtkActor()
        self.glyph_actor.SetMapper(self.glyph_mapper)

    def make_and_align_plane(self):
        print(getframeinfo(currentframe()).filename, getframeinfo(currentframe()).lineno)
        if self.plane_widget is None:
            print(93)
            self.make_plane()
            self.b_add_clipper.add_child_widget(self.plane_widget, "vtk")
        if self.b_add_clipper.get_combobox_text() == "Coupe libre":
            print(96)
            self.plane_widget.On()
            self.slider_axis_cilpping_plane.hide()
        elif self.b_add_clipper.get_combobox_text() == "Coupe en profondeur":
            print(99)
            self.plane.SetNormal([0, 0, 1])
            bounds = self.glyph_mapper.GetBounds()
            self.plane.SetOrigin(bounds[0], bounds[2], bounds[4])
            range_slider = [bounds[4], bounds[5]]
            self.slider_axis_cilpping_plane = QtWidgets.QSlider(QtCore.Qt.Vertical, self)
            self.slider_axis_cilpping_plane.setRange(range_slider[0], range_slider[1])
            self.slider_value = self.slider_axis_cilpping_plane.value()
            self.slider_axis_cilpping_plane.setFocusPolicy(QtCore.Qt.NoFocus)
            self.slider_axis_cilpping_plane.setMinimumSize(5, 200)
            self.slider_axis_cilpping_plane.setMaximumSize(5, 200)
            self.slider_axis_cilpping_plane.move(20, 150)
            self.slider_axis_cilpping_plane.valueChanged.connect(self.connect_slider_to_plane)
            self.slider_axis_cilpping_plane.show()
            self.b_add_clipper.add_child_widget(self.slider_axis_cilpping_plane, "pyqt")

    def make_plane(self):
        self.plane_widget = vtk.vtkPlaneWidget()
        self.plane_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
        self.plane_widget.SetProp3D(self.glyph_actor)
        self.plane_widget.SetPlaceFactor(1.25)  # Make the box 1.25x larger than the actor
        self.plane_widget.PlaceWidget()

        self.plane = vtk.vtkPlane()
        self.plane_widget.GetPlane(self.plane)

        # Connect the event to a function
        self.plane_widget.AddObserver("InteractionEvent", self.planeCallback)

        self.glyph_mapper.AddClippingPlane(self.plane)
        print(106)

    def planeCallback(self, obj, event):
        self.plane_widget.GetPlane(self.plane)

    def connect_slider_to_plane(self):
        self.plane.Push(self.slider_axis_cilpping_plane.value() - self.slider_value)
        print(self.plane.GetOrigin())
        self.slider_value = self.slider_axis_cilpping_plane.value()

    def color_by_parameter(self):
        # Source https://lorensen.github.io/VTKExamples/site/Python/Widgets/ScalarBarWidget/
        print(185)
        i = self.b_color_by_pameter.get_combobox_index()
        array = self.polydata.GetPointData().GetArray(i)
        array_length = array.GetNumberOfTuples()
        count, count2, count3 = 0, 0, 0
        for i in range(array_length):
            # print(i, array_length)
            if array.GetValue(i) != 1e-20 and array.GetValue(i) != 1:
                count3 += 1
                # print(array.GetValue(i))
            elif array.GetValue(i) == 1:
                count += 1
            else:
                count2 += 1
        print(99, count, count2)
        name = array.GetName()
        print(80, type(array), type(array.GetValueRange()))
        # scalar_range = array.GetValueRange()
        scalar_range = [array.GetValueRange()[0] * 2, array.GetValueRange()[1]]
        # scalar_range = [0.999, 1]
        print(82, scalar_range)
        # Create the lookup table
        lut = vtk.vtkLookupTable()
        lut.SetHueRange(0.15, 1)
        lut.SetSaturationRange(1, 1)
        lut.SetValueRange(1, 1)
        lut.SetAlphaRange(1, 1)
        print(118, lut.GetAlpha())
        # lut.SetAlpha(1)

        n_colors = lut.GetNumberOfTableValues()
        lut.Build()
        lut.SetTableValue(0, 0, 0, 0, 0)

        self.polydata.GetPointData().SetScalars(array)

        self.glyph_mapper.SetLookupTable(lut)
        print(128, self.glyph_mapper.GetLookupTable().GetAlphaRange())
        print(129, self.glyph_actor.GetProperty().GetOpacity())
        # self.glyph_actor.GetProperty().SetOpacity(1)
        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToHorizontal()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()
        self.b_color_by_pameter.add_child_widget(self.scalar_bar_widget, "vtk")

        self.show()


def main():
    app = QtWidgets.QApplication(sys.argv)
    myWindow = MyWindow("data/pcb_sgs_withtopo.csv")
    myWindow.show()

    # start the event loop
    try:
        sys.exit(app.exec_())  # clean exit
    except SystemExit as e:
        if e.code != 0:
            raise ()
        os._exit(0)


if __name__ == '__main__':
    main()
