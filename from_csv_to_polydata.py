"""
from csv makes polydata
"""
import os
import vtk

class Reader:

    def __init__(self, filepath="data/pcb_sgs_withtopo.csv"):
        self.filepath = filepath
        self.make_polydata_from_csv_file()

    def make_polydata_from_csv_file(self):
        points = vtk.vtkPoints()
        self.len_points = 0
        firstline = True
        filename = os.path.normcase(self.filepath)
        f = open(filename)
        polydata = vtk.vtkPolyData()
        x_value = None
        self.len_yz = 0
        y_value = None
        self.len_z = 0
        lines = []
        id = 0
        stop = False

        for line in f:

            if stop:
                # print(25)
                break
            if firstline:
                # skip first line, presumed to be a header
                firstline = False
                for pos, word in enumerate(line.split(";")):
                    if pos > 2:
                        new_array = vtk.vtkDoubleArray()
                        new_array.SetName(word)
                        new_array.SetNumberOfComponents(1)
                        polydata.GetPointData().AddArray(new_array)
            else:
                for pos, word in enumerate(line.split(";")):
                    # word.replace(",", ".")
                    try:
                        float(word.replace(",", "."))

                    except ValueError:
                        # print(44)
                        if pos == 0:
                            stop = True
                            break

                    if pos == 0:
                        # print(word, word.replace(",", "."))
                        x = float(word.replace(",", "."))
                        if x_value is None or x_value == x:
                            self.len_yz += 1
                            x_value = x
                        else:
                            x_value = False
                    if pos == 1:
                        y = float(word.replace(",", "."))
                        if y_value is None or y_value == y and x_value == x:
                            self.len_z += 1
                            # print(id - 1, y_value, y, word)
                            y_value = y
                    if pos == 2:
                        z = float(word.replace(",", "."))
                    if pos > 2:
                        array = polydata.GetPointData().GetArray(pos - 3)
                        try:
                            array.InsertNextValue(float(word.replace(",", ".")))
                            # print(99, count, count2)
                        except ValueError:
                            array.InsertNextValue(1e-20)
                if not stop:
                    points.InsertNextPoint(x, y, z)
                    self.len_points += 1
                # print(id, self.len_points, x, y, z)
            # print(id)
            id += 1

        self.len_y = int(self.len_yz / self.len_z)
        self.len_x = int(self.len_points / self.len_yz)
        print(49, "len_y", self.len_y, "\nlen_x", self.len_x, "\nlen_z", self.len_z, "\nlen_yz", self.len_yz, "\nlen_points", self.len_points)

        dx = abs(points.GetPoint(0)[0] - points.GetPoint(self.len_yz)[0])
        dy = abs(points.GetPoint(0)[1] - points.GetPoint(self.len_x)[1])
        dz = abs(points.GetPoint(0)[2] - points.GetPoint(1)[2])

        self.cell_size = [dx, dy, dz]
        # for i in range(self.len_points):
        #     print(83, i)
        #     polydata.InsertNextCell(vtk.VTK_VERTEX, 1, [i])
        #     print(85)
        #     # if i // self.len_z == (i + 1) // self.len_z:
        #     #     print(87, i)
        #     #     polydata.InsertNextCell(vtk.VTK_LINE, 2, [i, i + 1])
        #     #     print(89, i)
        #     # if i // self.len_yz == (i + self.len_z) // self.len_yz:
        #     #     polydata.InsertNextCell(vtk.VTK_LINE, 2, [i, i + self.len_z])
        #     # if i + self.len_yz < self.len_points:
        #     #     polydata.InsertNextCell(vtk.VTK_LINE, 2, [i, i + self.len_yz])
        # print(84)
        polydata.SetPoints(points)
        self.polydata = polydata
