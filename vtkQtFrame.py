from PyQt5 import QtGui
from PyQt5 import QtWidgets
import vtk
import vtk.qt.QVTKRenderWindowInteractor


class VtkQtFrame(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)

        self.VTKRenderer = vtk.vtkRenderer()
        self.VTKRenderer.SetBackground(255, 255, 255)
        self.VTKRenderer.SetViewport(0, 0, 1, 1)

        self.VTKRenderWindow = vtk.vtkRenderWindow()
        self.VTKRenderWindow.AddRenderer(self.VTKRenderer)
        self.VTKRenderWindowInteractor = vtk.qt.QVTKRenderWindowInteractor.QVTKRenderWindowInteractor(self, rw=self.VTKRenderWindow)
        self.layout.addWidget(self.VTKRenderWindowInteractor)

        self.VTKCamera = vtk.vtkCamera()
        self.VTKCamera.SetClippingRange(0.1, 1000)
        self.VTKRenderer.SetActiveCamera(self.VTKCamera)

        self.VTKInteractorStyleSwitch = vtk.vtkInteractorStyleSwitch()
        self.VTKInteractorStyleSwitch.SetCurrentStyleToTrackballCamera()
        self.VTKRenderWindowInteractor.SetInteractorStyle(self.VTKInteractorStyleSwitch)
        # self.VTKRenderWindowInteractor.Initialize()
        # self.VTKRenderWindowInteractor.Start()
        # self.VTKRenderWindowInteractor.ReInitialize()
