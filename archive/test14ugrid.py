#!/usr/bin/env python

"""
Example with unstructured grid.
"""
import os
import vtk


def make_ugrid_from_csv_file(filepath="data/exemple_fichier_import_2D.csv"):
    points = vtk.vtkPoints()
    len_points = 0
    firstline = True
    filename = os.path.normcase(filepath)
    f = open(filename)
    ugrid = vtk.vtkUnstructuredGrid()

    for line in f:
        if firstline:
            # skip first line, presumed to be a header
            firstline = False
            for pos, word in enumerate(line.split(";")):
                # Works if no z!!!
                if pos >= 2:
                    new_array = vtk.vtkDoubleArray()
                    new_array.SetName(word)
                    ugrid.GetPointData().AddArray(new_array)
                    # array.SetNumberOfComponents(1)
        else:
            for pos, word in enumerate(line.split(";")):
                print(word)
                if pos == 0:
                    x = float(word)
                if pos == 1:
                    y = float(word)
                # if pos == 2:
                #     z = float(word)
                if pos >= 2:
                    array = ugrid.GetPointData().GetArray(pos - 2)
                    array.InsertNextValue(float(word))
            points.InsertNextPoint(x, y, 0)
            len_points += 1
            # print(35, x, y, 0, array)



    # scalar_range = array.GetValueRange()
    # lut = vtk.vtkLookupTable()
    # lut.SetRange(scalar_range)
    # lut.SetNumberOfTableValues(255)
    # for i in range(255):
    #     lut.SetTableValue(i, i / 255.0, 0.0, 1 - i / 255.0, 1.0)
    # lut.Build()


    # colors = vtk.vtkNamedColors()



    for i in range(len_points):
        ugrid.InsertNextCell(vtk.VTK_VERTEX, 1, [i])

    ugrid.SetPoints(points)
    # ugrid.GetPointData().AddArray(array)
    # ugrid.GetPointData().SetScalars(array)

    # ugridMapper = vtk.vtkDataSetMapper()
    # ugridMapper.SetInputData(ugrid)
    # ugridMapper.UseLookupTableScalarRangeOff()
    # ugridMapper.SetLookupTable(lut)
    # ugridMapper.SetScalarRange(scalar_range)
    print(73, ugrid.GetPointData())

    # ugridActor = vtk.vtkActor()
    # ugridActor.SetMapper(ugridMapper)
    # ugridActor.GetProperty().SetColor(colors.GetColor3d("Peacock"))
    # ugridActor.GetProperty().EdgeVisibilityOn()

    # renderer.AddActor(ugridActor)
    # renderer.SetBackground(colors.GetColor3d("Black"))
    #
    # renderer.ResetCamera()
    # renderer.GetActiveCamera().Elevation(60.0)
    # renderer.GetActiveCamera().Azimuth(30.0)
    # renderer.GetActiveCamera().Dolly(1.2)
    #
    # renWin.SetSize(640, 480)
    #
    # # Interact with the data.
    # renWin.Render()
    #
    # iren.Start()
    return ugrid




if __name__ == "__main__":
    make_ugrid_from_csv_file()
