# from PyQt5 import QtWidgets
# import vtk
# import vtkQtFrame
# import os
# import sys
#
#
#
# # Call back function to resize the cone
# class MyWindow:
#     def __init__(self):
#         self.main()
#
#     def main(self):
#         app = QtWidgets.QApplication(sys.argv)
#         render_widget = vtkQtFrame.VtkQtFrame()
#
#         colors = vtk.vtkNamedColors()
#
#         # Create a vtkImageData
#         img = vtk.vtkImageData()
#         img.SetDimensions(10, 5, 2)
#         img.SetSpacing(1, 1, 1)
#         img.SetOrigin(0, 0, 0)
#         img_mapper = vtk.vtkDataSetMapper()
#         img_mapper.SetInputData(img)
#         # imgdataMapper.SetInputData(self.imageData)
#         img_actor = vtk.vtkActor()
#         img_actor.SetMapper(img_mapper)
#         img_actor.GetProperty().SetColor(colors.GetColor3d("BurlyWood"))
#         img_actor.GetProperty().SetRepresentationToWireframe()
#         render_widget.VTKRenderer.AddActor(img_actor)
#
#         # A plane widget
#         self.plane_widget = vtk.vtkImagePlaneWidget()
#         self.plane_widget.SetInteractor(render_widget.VTKRenderWindowInteractor)
#         self.plane_widget.SetProp3D(img_actor)
#         self.plane_widget.SetPlaceFactor(1.25)  # Make the box 1.25x larger than the actor
#         self.plane_widget.PlaceWidget()
#         self.plane_widget.On()
#
#         # Create plane from plane_widget
#         self.plane = vtk.vtkPlane()
#         self.plane.SetOrigin(self.plane_widget.GetOrigin())
#         self.plane.SetNormal(self.plane_widget.GetNormal())
#
#         # Create the clipped version of the data
#         clipper = vtk.vtkClipDataSet()
#         clipper.SetInputData(img)
#         clipper.SetClipFunction(self.plane)
#         clipper_mapper = vtk.vtkDataSetMapper()
#         clipper_mapper.SetInputConnection(clipper.GetOutputPort())
#         # imgdataMapper.SetInputData(self.imageData)
#         clipper_actor = vtk.vtkActor()
#         clipper_actor.SetMapper(clipper_mapper)
#         clipper_actor.GetProperty().SetColor(colors.GetColor3d("BurlyWood"))
#         clipper_actor.GetProperty().SetRepresentationToWireframe()
#         render_widget.VTKRenderer.AddActor(clipper_actor)
#
#         # Connect the event to a function
#         self.plane_widget.AddObserver("InteractionEvent", self.planeCallback)
#
#         # Display in custom vtk Qt Window
#         window = QtWidgets.QMainWindow()
#
#         render_widget.VTKRenderer.SetBackground(0, 0, 0)
#         render_widget.VTKRenderer.ResetCamera()
#
#         window.setCentralWidget(render_widget)
#         window.show()
#         # start the event loop
#         try:
#             sys.exit(app.exec_())  # clean exit
#         except SystemExit as e:
#             if e.code != 0:
#                 raise ()
#             os._exit(0)
#         # interactor.Initialize()
#         # renwin.Render()
#
#
#     def planeCallback(self, obj, event):
#         self.plane.SetOrigin(self.plane_widget.GetOrigin())
#         self.plane.SetNormal(self.plane_widget.GetNormal())
#         print("Moved the plane.")
#
#
# # interactor.Start()
# if __name__ == '__main__':
#     w = MyWindow()





#### Test for cropping widget


from PyQt5 import QtWidgets
import vtk
import vtkQtFrame
import os
import sys


class MyWindow:
    def __init__(self):
        self.main()

    def main(self):
        app = QtWidgets.QApplication(sys.argv)
        render_widget = vtkQtFrame.VtkQtFrame()

        colors = vtk.vtkNamedColors()

        # Create a vtkImageData
        img = vtk.vtkImageData()
        img.SetDimensions(10, 5, 3)
        img.SetSpacing(1, 1, 1)
        img.SetOrigin(0, 0, 0)
        img_mapper = vtk.vtkDataSetMapper()
        img_mapper.SetInputData(img)
        # imgdataMapper.SetInputData(self.imageData)

        img_actor = vtk.vtkActor()
        img_actor.SetMapper(img_mapper)
        img_actor.GetProperty().SetColor(colors.GetColor3d("BurlyWood"))
        img_actor.GetProperty().SetRepresentationToWireframe()
        render_widget.VTKRenderer.AddActor(img_actor)


        # Create a volume
        volume_mapper = vtk.vtkSmartVolumeMapper()
        volume_mapper.SetInputData(img)

        # vtkImageCroppingRegionsWidget
        self.cropping_widget = vtk.vtkImageCroppingRegionsWidget()
        self.cropping_widget.SetInputData(img)
        self.cropping_widget.SetInteractor(render_widget.VTKRenderWindowInteractor)
        self.cropping_widget.SetSliceOrientationToXY()
        # self.cropping_widget.SetPlanePositions(0,40,0,50,0,0)

        self.cropping_widget.On()

        # Display in custom vtk Qt Window
        window = QtWidgets.QMainWindow()

        render_widget.VTKRenderer.SetBackground(0, 0, 0)
        render_widget.VTKRenderer.ResetCamera()

        window.setCentralWidget(render_widget)
        window.show()
        # start the event loop
        try:
            sys.exit(app.exec_())  # clean exit
        except SystemExit as e:
            if e.code != 0:
                raise ()
            os._exit(0)
        # interactor.Initialize()
        # renwin.Render()


    def planeCallback(self, obj, event):
        self.plane.SetOrigin(self.plane_widget.GetOrigin())
        self.plane.SetNormal(self.plane_widget.GetNormal())
        print("Moved the plane.")


# interactor.Start()
if __name__ == '__main__':
    w = MyWindow()







### JS code for cropping widget


# import vtk
#
# import vtkFullScreenRenderWindow from 'vtk.js/Sources/Rendering/Misc/FullScreenRenderWindow';
# import vtkHttpDataSetReader from 'vtk.js/Sources/IO/Core/HttpDataSetReader';
# import vtkVolumeMapper from 'vtk.js/Sources/Rendering/Core/VolumeMapper';
# import vtkImageMapper from 'vtk.js/Sources/Rendering/Core/ImageMapper';
# import vtkImageSlice from 'vtk.js/Sources/Rendering/Core/ImageSlice';
# import vtkInteractorStyleImage from 'vtk.js/Sources/Interaction/Style/InteractorStyleImage';
# import vtkInteractorStyleTrackballCamera from 'vtk.js/Sources/Interaction/Style/InteractorStyleTrackballCamera';
# import vtkImageCroppingRegionsWidget from 'vtk.js/Sources/Interaction/Widgets/ImageCroppingRegionsWidget';
#
# import controlPanel from './controlPanel.html';

# // ----------------------------------------------------------------------------
# // Standard rendering code setup
# // ----------------------------------------------------------------------------

# fullScreenRenderer = vtk.vtkFullScreenRenderWindow()
# renderer = vtk.fullScreenRenderer.getRenderer()
# renderWindow = vtk.fullScreenRenderer.getRenderWindow()
# # /* eslint-disable */
# interactorStyle2D = vtk.vtkInteractorStyleImage()
# interactorStyle3D = vtk.vtkInteractorStyleTrackballCamera()
# # // switch to using interactorStyle2D if you want 2D controls
# renderWindow.getInteractor().setInteractorStyle(interactorStyle3D)
# # // set the current image number to the first image
# # // interactorStyle2D.setCurrentImageNumber(0);
# # /* eslint-enable */
# fullScreenRenderer.addController(vtk.controlPanel)
# # // renderer.getActiveCamera().setParallelProjection(true);

# // ----------------------------------------------------------------------------
# // Helper methods for setting up control panel
# // ----------------------------------------------------------------------------

# def setupControlPanel(data, imageMapper):
#     sliceInputs = [vtk.querySelector('.sliceI'), vtk.querySelector('.sliceJ'), vtk.querySelector('.sliceK')]
#     viewAxisInput = vtk.querySelector('.viewAxis')
#
#     extent = data.getExtent()
#     for idx, el in enumerate(sliceInputs):
#         lowerExtent = extent[idx * 2]
#         upperExtent = extent[idx * 2 + 1]
#         el.setAttribute('min', lowerExtent)
#         el.setAttribute('max', upperExtent)
#         el.setAttribute('value', (upperExtent - lowerExtent) / 2)
#
#     viewAxisInput.value = 'IJKXYZ'[imageMapper.getSlicingMode()]
#
#
#     for idx, el in enumerate(sliceInputs):
#         el.addEventListener('input', (ev) =
#       const sliceMode = sliceInputs.indexOf(el);
#       if (imageMapper.getSlicingMode() === sliceMode) {
#         imageMapper.setSlice(Number(ev.target.value));
#         renderWindow.render();
#       }
#     });
#   });
#
#   viewAxisInput.addEventListener('input', (ev) => {
#     const sliceMode = 'IJKXYZ'.indexOf(ev.target.value);
#     imageMapper.setSlicingMode(sliceMode);
#     const slice = sliceInputs[sliceMode].value;
#     imageMapper.setSlice(slice);
#
#     const camPosition = renderer
#       .getActiveCamera()
#       .getFocalPoint()
#       .map((v, idx) => (idx === sliceMode ? v + 1 : v));
#     const viewUp = [0, 0, 0];
#     viewUp[(sliceMode + 2) % 3] = 1;
#     renderer.getActiveCamera().set({ position: camPosition, viewUp });
#     renderer.resetCamera();
#
#     renderWindow.render();
#   });
# }
#
# // ----------------------------------------------------------------------------
# // Create widget
# // ----------------------------------------------------------------------------
# const widget = vtkImageCroppingRegionsWidget.newInstance();
# widget.setInteractor(renderWindow.getInteractor());
#
# // Demonstrate cropping planes event update
# widget.onCroppingPlanesChanged((planes) => {
#   console.log('planes changed:', planes);
# });
#
# // called when the volume is loaded
# function setupWidget(volumeMapper, imageMapper) {
#   widget.setVolumeMapper(volumeMapper);
#   widget.setHandleSize(12); // in pixels
#   widget.setEnabled(true);
#
#   // demonstration of setting various types of handles
#   widget.setFaceHandlesEnabled(true);
#   // widget.setEdgeHandlesEnabled(true);
#   widget.setCornerHandlesEnabled(true);
#
#   renderWindow.render();
# }
#
# // ----------------------------------------------------------------------------
# // Set up volume
# // ----------------------------------------------------------------------------
# const volumeMapper = vtkVolumeMapper.newInstance();
# const imageMapper = vtkImageMapper.newInstance();
# const actor = vtkImageSlice.newInstance();
# actor.setMapper(imageMapper);
# renderer.addViewProp(actor);
#
# const reader = vtkHttpDataSetReader.newInstance({ fetchGzip: true });
# reader
#   .setUrl(`${__BASE_PATH__}/data/volume/headsq.vti`, { loadData: true })
#   .then(() => {
#     const data = reader.getOutputData();
#
#     volumeMapper.setInputData(data);
#     imageMapper.setInputData(data);
#
#     // create our cropping widget
#     setupWidget(volumeMapper, imageMapper);
#
#     // After creating our cropping widget, we can now update our image mapper
#     // with default slice orientation/mode and camera view.
#     const sliceMode = vtkImageMapper.SlicingMode.K;
#     const viewUp = [0, 1, 0];
#
#     imageMapper.setSlicingMode(sliceMode);
#     imageMapper.setSlice(0);
#
#     const camPosition = renderer
#       .getActiveCamera()
#       .getFocalPoint()
#       .map((v, idx) => (idx === sliceMode ? v + 1 : v));
#     renderer.getActiveCamera().set({ position: camPosition, viewUp });
#
#     // setup control panel
#     setupControlPanel(data, imageMapper);
#
#     renderer.resetCamera();
#     renderWindow.render();
#   });
#
# // -----------------------------------------------------------
# // Make some variables global so that you can inspect and
# // modify objects in your browser's developer console:
# // -----------------------------------------------------------
#
# global.renderer = renderer;
# global.renderWindow = renderWindow;
# global.widget = widget;