from PyQt5 import QtWidgets, QtCore
import vtk
import sys
import os
import vtkQtFrame
# from cvs_reader_from_web import polyDataOutput
from from_csv_to_polydata import make_polydata_from_csv_file


# from cvs_reader_from_web import polyDataOutput

class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        dw = QtWidgets.QDesktopWidget()
        x = dw.width()
        y = dw.height()
        self.resize(x * 0.5, y * 0.7)
        self.initUI()

    def initUI(self):
        self.render_widget = vtkQtFrame.VtkQtFrame()

        # self.ugrid, self.points = make_ugrid_from_csv_file()

        self.polydata = make_polydata_from_csv_file()
        self.polydata_mapper = vtk.vtkDataSetMapper()
        self.polydata_mapper.SetInputData(self.polydata)

        self.cubeSource = vtk.vtkCubeSource()
        size = 2
        self.cubeSource.SetXLength(size)
        self.cubeSource.SetYLength(size)
        self.cubeSource.SetZLength(size / 5)
        # This allows to put cubes centered on the points
        self.glyph3D = vtk.vtkGlyph3D()
        self.glyph3D.SetSourceConnection(self.cubeSource.GetOutputPort())
        self.glyph3D.SetInputDataObject(self.polydata)

        self.glyph3D.SetColorModeToColorByScalar()
        self.glyph3D.ScalingOff()
        self.glyph3D.Update()

        self.glyph_mapper = vtk.vtkDataSetMapper()
        # self.img_mapper.SetInputData(self.img)
        self.glyph_mapper.SetInputConnection(self.glyph3D.GetOutputPort())
        self.glyph_actor = vtk.vtkActor()
        self.glyph_actor.SetMapper(self.glyph_mapper)

        self.render_widget.VTKRenderer.AddActor(self.glyph_actor)

        self.render_widget.VTKRenderer.SetBackground(0, 0, 0)

        # reset the camera and set anti-aliasing to 2x
        self.render_widget.VTKRenderer.ResetCamera()

        # add and show
        self.setCentralWidget(self.render_widget)

        self.button_ = QtWidgets.QCheckBox(self)
        self.button_.setStyleSheet("color: white")
        self.button_.setText("Colorer par parametre")
        self.button_.stateChanged.connect(self.add_color_combobox)
        self.combobox_color_by_parameters = None

        # self.combobox_color_by_parameters.show()
        self.b_add_clipper = QtWidgets.QCheckBox(self)
        self.b_add_clipper.setStyleSheet("color: white")
        self.b_add_clipper.setText("Ajouter un plan de coupe")
        self.b_add_clipper.stateChanged.connect(self.add_clipping_plane)
        self.b_add_clipper.move(0, 50)
        self.plane = None

        self.render_widget.VTKRenderWindow.Render()

    def add_color_combobox(self):
        print(78)
        if self.button_.isChecked() and self.combobox_color_by_parameters is None:
            print(79)
            self.combobox_color_by_parameters = QtWidgets.QComboBox(self)
            for i in range(self.polydata.GetPointData().GetNumberOfArrays()):
                name_array = str(self.polydata.GetPointData().GetArrayName(i))
                self.combobox_color_by_parameters.addItem(name_array)
            self.combobox_color_by_parameters.currentTextChanged.connect(self.colorByParameter)
            self.combobox_color_by_parameters.move(0, 100)
            self.combobox_color_by_parameters.show()
        elif self.button_.isChecked() and self.combobox_color_by_parameters is not None:
            print(88)
            self.combobox_color_by_parameters.show()
        else:
            print(91)
            self.combobox_color_by_parameters.hide()
            self.scalar_bar_widget.Off()

    def add_clipping_plane(self):
        print(165)
        if self.b_add_clipper.isChecked() and self.glyph_actor.GetVisibility():
            print(167)
            self.combobox_axis_cilpping_plane = QtWidgets.QComboBox(self)
            self.combobox_axis_cilpping_plane.addItems(["X axis", "Y axis", "Z Axis"])
            self.combobox_axis_cilpping_plane.move(0, 150)
            self.combobox_axis_cilpping_plane.show()
            self.combobox_axis_cilpping_plane.currentIndexChanged.connect(self.align_plan_on_axis)
            print(188)
            if self.plane is None:
                print(175)
                bounds = self.glyph_mapper.GetBounds()
                print(177)
                # Create plane from plane_widget
                self.plane_widget = vtk.vtkPlaneWidget()
                self.plane_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
                self.plane_widget.SetProp3D(self.glyph_actor)
                self.plane_widget.SetPlaceFactor(1.25)  # Make the box 1.25x larger than the actor
                self.plane_widget.PlaceWidget()
                # self.plane_widget.SetOrigin(bounds[0], bounds[2], bounds[4])  # Make the box 1.25x larger than the actor
                # self.plane_widget.SetNormal([1, 0, 0])
                print(185)

                self.plane = vtk.vtkPlane()
                self.plane_widget.GetPlane(self.plane)
                print(189)
                # Connect the event to a function
                self.plane_widget.AddObserver("InteractionEvent", self.planeCallback)

                # self.plane = vtk.vtkPlane()
                # self.plane.SetOrigin(bounds[0], bounds[2], bounds[4])
                # self.plane.SetNormal([1, 0, 0])

                self.glyph_mapper.AddClippingPlane(self.plane)
                print(185)
            self.plane_widget.On()
        else:
            self.combobox_axis_cilpping_plane.hide()
            try:
                self.slider_axis_cilpping_plane.hide()
            except:
                pass
        print(216)

        # Following line updates the renderer so that all the action above are displayed
        # self.render_widget.VTKRenderWindowInteractor.Start()
        self.show()
        print(220)

    def colorByParameter(self):
        # Source https://lorensen.github.io/VTKExamples/site/Python/Widgets/ScalarBarWidget/
        i = self.combobox_color_by_parameters.currentIndex()
        array = self.polydata.GetPointData().GetArray(i)
        array_length = array.GetNumberOfTuples()
        count, count2, count3 = 0, 0, 0
        for i in range(array_length):
            # print(i, array_length)
            if array.GetValue(i) != 1e-20 and array.GetValue(i) != 1:
                count3 += 1
                # print(array.GetValue(i))
            elif array.GetValue(i) == 1:
                count += 1
            else:
                count2 += 1
        print(99, count, count2)
        name = array.GetName()
        print(80, type(array), type(array.GetValueRange()))
        # scalar_range = array.GetValueRange()
        scalar_range = [array.GetValueRange()[0] * 2, array.GetValueRange()[1]]
        # scalar_range = [0.999, 1]
        print(82, scalar_range)
        # Create the lookup table
        lut = vtk.vtkLookupTable()
        lut.SetHueRange(0.15, 1)
        lut.SetSaturationRange(1, 1)
        lut.SetValueRange(1, 1)
        lut.SetAlphaRange(1, 1)
        print(118, lut.GetAlpha())
        # lut.SetAlpha(1)

        n_colors = lut.GetNumberOfTableValues()
        lut.Build()
        lut.SetTableValue(0, 0, 0, 0, 0)

        self.polydata.GetPointData().SetScalars(array)

        self.glyph_mapper.SetLookupTable(lut)
        print(128, self.glyph_mapper.GetLookupTable().GetAlphaRange())
        print(129, self.glyph_actor.GetProperty().GetOpacity())
        # self.glyph_actor.GetProperty().SetOpacity(1)
        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToHorizontal()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()

        # self.render_widget.VTKRenderWindowInteractor.Start()
        # self.render_widget.VTKRenderWindowInteractor.Initialize()
        # self.render_widget.VTKRenderWindowInteractor.ProcessEvents()
        # self.render_widget.VTKRenderWindowInteractor.ReInitialize()
        self.show()

        # self.polydata.SetLookUpTable().

    def planeCallback(self, obj, event):
        self.plane_widget.GetPlane(self.plane)

    def align_plan_on_axis(self):
        # normal = self.plane_widget.GetNormal()
        # self.slider_value = self.plane_widget.GetOrigin()
        # print(173, self.slider_value, normal)
        self.slider_axis_cilpping_plane = QtWidgets.QSlider(QtCore.Qt.Vertical, self)
        bounds = self.glyph_mapper.GetBounds()
        print(176, bounds)
        if self.combobox_axis_cilpping_plane.currentText() == "X axis":
            normal = [1, 0, 0]
            range_slider = [bounds[0], bounds[1]]
        elif self.combobox_axis_cilpping_plane.currentText() == "Y axis":
            normal = [0, 1, 0]
            range_slider = [bounds[2], bounds[3]]
        else:
            normal = [0, 0, -1]
            range_slider = [bounds[4], bounds[5]]

        print(187, normal, range_slider)

        self.plane.SetNormal(normal[0], normal[1], normal[2])
        self.plane.SetOrigin(bounds[0], bounds[2], bounds[4])

        self.slider_axis_cilpping_plane.setRange(range_slider[0], range_slider[1])
        self.slider_value = self.slider_axis_cilpping_plane.value()
        self.slider_axis_cilpping_plane.setFocusPolicy(QtCore.Qt.NoFocus)
        self.slider_axis_cilpping_plane.setMinimumSize(5, 200)
        self.slider_axis_cilpping_plane.setMaximumSize(5, 200)
        self.slider_axis_cilpping_plane.move(20, 150)
        self.slider_axis_cilpping_plane.valueChanged.connect(self.connect_slider_to_plane)
        self.slider_axis_cilpping_plane.show()
        # self.plane.SetOrigin(self.slider_value[0], self.slider_value[1], self.slider_value[2])
        # self.plane_widget.Update()

    def connect_slider_to_plane(self):
        self.plane.Push(self.slider_axis_cilpping_plane.value() - self.slider_value)
        print(self.plane.GetOrigin())
        self.slider_value = self.slider_axis_cilpping_plane.value()


def main():
    app = QtWidgets.QApplication(sys.argv)
    myWindow = MyWindow()
    myWindow.show()

    # start the event loop
    try:
        sys.exit(app.exec_())  # clean exit
    except SystemExit as e:
        if e.code != 0:
            raise ()
        os._exit(0)


if __name__ == '__main__':
    main()
