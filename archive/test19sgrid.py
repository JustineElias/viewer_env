import vtk
import os




def make_image_from_csv_file(filepath="data/pcb_sgs_withtopo.csv"):
    points = vtk.vtkPoints()
    len_points = 0
    firstline = True
    filename = os.path.normcase(filepath)
    f = open(filename)
    # polydata = vtk.vtkPolyData()
    sgrid = vtk.vtkStructuredGrid()
    x_value = None
    len_yz = 0
    y_value = None
    len_z = 0
    z_value = None
    lines = []
    id = 0
    stop = False

    for line in f:

        if stop:
            # print(25)
            break
        if firstline:
            # skip first line, presumed to be a header
            firstline = False
            for pos, word in enumerate(line.split(";")):
                # Works if no z!!!
                if pos > 2:
                    new_array = vtk.vtkDoubleArray()
                    new_array.SetName(word)
                    sgrid.GetCellData().AddArray(new_array)
                    # array.SetNumberOfComponents(1)
        else:
            for pos, word in enumerate(line.split(";")):
                # print(33, word)
                # word.replace(",", ".")
                try:
                    float(word.replace(",", "."))
                except ValueError:
                    if pos == 0:
                        stop = True
                        break

                if pos == 0:
                    # print(word, word.replace(",", "."))
                    x = float(word.replace(",", "."))
                    if x_value is None or x_value == x:
                        len_yz += 1
                        x_value = x
                    elif x_value:
                        delta_x = x - x_value
                        x_value = False
                if pos == 1:
                    y = float(word.replace(",", "."))
                    if y_value is None or y_value == y and x_value == x:
                        len_z += 1
                        # print(id - 1, y_value, y, word)
                        y_value = y
                    elif y_value:
                        delta_y = y - y_value
                        y_value = False
                if pos == 2:
                    z = float(word.replace(",", "."))
                    if z_value is None:
                        z_value = z
                    elif z_value:
                        delta_z = z - z_value
                        z_value = False
                if pos > 2:
                    array = sgrid.GetCellData().GetArray(pos - 3)
                    try:
                        array.InsertNextValue(float(word))
                    except ValueError:
                        array.InsertNextValue(1e-20)
            if not stop:
                # points.InsertNextPoint(x, y, z)
                len_points += 1
            # print(id, len_points, x, y, z)
        # print(id)
        id += 1

    len_y = int(len_yz / len_z)
    len_x = int(len_points / len_yz)
    print(49, "\nlen_y", len_y, "\nlen_x", len_x, "\nlen_z", len_z, "\nlen_yz", len_yz, "\nlen_points", len_points)
    print("\ndelta_x", delta_x, "\ndelta_y", delta_y, "\ndelta_z", delta_z)
    colors = vtk.vtkNamedColors()

    # sgrid = vtk.vtkImageData()
    sgrid.SetDimensions(len_x, len_y, len_z)
    sgrid.SetSpacing(delta_x, delta_y, delta_z)
    # sgrid.AllocateScalars(vtk.VTK_DOUBLE, polydata.GetPointData().GetNumberOfArrays())

    # dims = sgrid.GetDimensions()
    # array = polydata.GetPointData().GetArray(1)
    print(70)

    return sgrid


    # Fill every entry of the image data with '2.0'
    # for x in range(dims[0]):
    #     for y in range(dims[1]):
    #         for z in range(dims[2]):
    #             print(75, array)
    #             for a in range(polydata.GetPointData().GetNumberOfArrays()):
                    # sgrid.SetScalarComponentFromDouble(x, y, z, a, polydata.GetPointData().GetArray(a)[z + y * len_z + x * len_yz])


    # Convert the image to a polydata
    # imageDataGeometryFilter = vtk.vtkImageDataGeometryFilter()
    # imageDataGeometryFilter.SetInputData(sgrid)
    # imageDataGeometryFilter.Update()
    #
    # mapper = vtk.vtkPolyDataMapper()
    # mapper.SetInputConnection(imageDataGeometryFilter.GetOutputPort())


    # actor = vtk.vtkActor()
    # actor.SetMapper(mapper)
    # actor.GetProperty().SetPointSize(3)
    #
    # # Setup rendering
    # renderer = vtk.vtkRenderer()
    # renderer.AddActor(actor)
    # renderer.SetBackground(colors.GetColor3d('White'))
    # renderer.ResetCamera()
    #
    # renderWindow = vtk.vtkRenderWindow()
    # renderWindow.AddRenderer(renderer)
    #
    # renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    #
    # renderWindowInteractor.SetRenderWindow(renderWindow)
    # renderWindowInteractor.Initialize()
    # renderWindowInteractor.Start()


if __name__ == '__main__':
    make_image_from_csv_file()