import vtk
import os

pts = vtk.vtkPoints()
firstline = True
filename = os.path.normcase("../data/pcb_sgs_withtopo.csv")
f = open(filename)
# polyData = vtk.vtkPolyData()
# polyDataOutput = polyData.GetOutputPort()
polyDataOutput = vtk.vtkPolyData()

for line in f:
    if firstline:
        # skip first line, presumed to be a header
        firstline = False
        for pos, word in enumerate(line.split(";")):
            if pos > 2:
                newArray = vtk.vtkDoubleArray()
                newArray.SetName(word)
                newArray.SetNumberOfComponents(1)
                polyDataOutput.GetPointData().AddArray(newArray)
    else:
        for pos, word in enumerate(line.split(";")):
            print(word)
            w = word.replace(",", ".")
            if pos == 0:
                x = float(w)
            if pos == 1:
                y = float(w)
            if pos == 2:
                z = float(w)
            if pos > 2:
                array = polyDataOutput.GetPointData().GetArray(pos - 3)
                array.InsertNextValue(float(w))
        pts.InsertNextPoint(x, y, z)
        print(35, x, y, z, array)
polyDataOutput.SetPoints(pts)
unstructured_grid = vtk.vtkUnstructuredGrid()
unstructured_grid.SetPoints(pts)

# help(polyDataOutput)
