import vtk
import numpy


class SphereActor(vtk.vtkActor):
    def __init__(self, rad, res, r, c):
        self.pos = numpy.array(r)
        self.source = vtk.vtkSphereSource()
        self.source.SetRadius(rad)
        self.source.SetPhiResolution(res)
        self.source.SetThetaResolution(res)
        self.source.SetCenter(r[0], r[1], r[2])
        self.Mapper = vtk.vtkPolyDataMapper()
        self.Mapper.SetInputData(self.source.GetOutput())
        self.SetMapper(self.Mapper)
        self.GetProperty().SetColor((c[0], c[1], c[2]))

    def move_to(self, r):
        self.pos = numpy.array(r)
        self.source.SetCenter(r[0], r[1], r[2])

    def set_color(self, color):
        self.GetProperty().SetColor(color)

    def set_rad(self, rad):
        self.source.SetRadius(rad)

    def get_pos(self):
        return self.pos
