from PyQt5 import QtWidgets
import vtk
import sys
import os
import vtkQtFrame
from archive.test18imagedata import make_image_from_csv_file

def planeCallback(obj, event):
    t = vtk.vtkTransform()
    obj.GetTransform(t)
    obj.GetProp3D().SetUserTransform(t)

class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.initUI()

    def initUI(self):
        self.render_widget_ = vtkQtFrame.VtkQtFrame()

        self.imageData = make_image_from_csv_file()


        self.imgdataMapper = vtk.vtkDataSetMapper()
        self.imgdataMapper.SetInputData(self.imageData)
        # self.imgdataMapper.AddClippingPlane(self.clipping_plane_z)

        self.imgdata_actor = vtk.vtkActor()
        self.imgdata_actor.SetMapper(self.imgdataMapper)

        # self.imgdata_actor = vtk.vtkImageActor()
        # self.imgdata_actor.GetMapper().SetInputConnection(self.imgdataMapper.GetOutputPort())


        self.render_widget_.VTKRenderer.AddActor(self.imgdata_actor)

        self.render_widget_.VTKRenderer.SetBackground(0, 0, 0)

        self.render_widget_.VTKRenderer.ResetCamera()

        # Clipping plane
        self.clipping_plane_z = vtk.vtkImagePlaneWidget()
        # self.clipping_plane_z = vtk.vtkBoxWidget()
        # self.clipping_plane_z.SetOrigin(0, 0, 0)
        # self.clipping_plane_z.SetNormalToZAxis(True)
        # self.clipping_plane_z.SetProp3D(self.imgdata_actor)
        # self.clipping_plane_z.SetInputData(self.imageData)
        # self.clipping_plane_z.SetPlaneOrientationToZAxes()
        self.clipping_plane_z.SetInteractor(self.render_widget_.VTKRenderWindowInteractor)
        # self.clipping_plane_z.SetPlaceFactor(1.25)
        # self.clipping_plane_z.GenerateTexturePlane()
        # self.clipping_plane_z.PlaceWidget()
        # self.clipping_plane_z.UpdatePlacement()
        self.clipping_plane_z.On()
        # self.clipping_plane_z.AddObserver("InteractionEvent", planeCallback)

        # add and show
        self.setCentralWidget(self.render_widget_)

        self.button_ = QtWidgets.QPushButton(self)
        self.button_.setText("Colorer par parametre")
        self.button_.clicked.connect(self.colorByParameter)

        self.show()

    def colorByParameter(self):
        ##do that
        print("Clicked colorByParameter")
        # Source https://lorensen.github.io/VTKExamples/site/Python/Widgets/ScalarBarWidget/

        # scalar_range = self.polydata.GetScalarRange()       # Original line
        # scalar_range = self.polydata.GetPointData().GetArray(0).GetValueRange()
        array = self.imageData.GetCellData().GetArray(1)
        print(59, type(array))
        scalar_range = array.GetValueRange()
        print(61)
        lut = vtk.vtkLookupTable()
        print(63)
        lut.SetRange(scalar_range)
        print(65)
        lut.SetNumberOfTableValues(255)
        print(67)
        for i in range(255):
            lut.SetTableValue(i, i / 255.0, 0.0, 1 - i / 255.0, 1.0)
        print(70)
        lut.Build()
        print(72)
        self.imageData.GetCellData().SetScalars(array)
        print(74)
        self.imgdataMapper.UseLookupTableScalarRangeOff()
        print(76)
        self.imgdataMapper.SetLookupTable(lut)
        print(78)
        self.imgdataMapper.SetScalarRange(scalar_range)
        print(80)

        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToHorizontal()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget_.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()

        self.render_widget_.VTKRenderWindowInteractor.Initialize()
        # self.render_widget_.VTKRenderWindowInteractor.Start()
        # self.render_widget_.VTKRenderWindowInteractor.ReInitialize()
        # self.show()

        # self.polydata.SetLookUpTable().

    # def make_buttons_
    def get_scalar_range(self, array):
        null_value = 1e-20


def main():
    app = QtWidgets.QApplication(sys.argv)
    myWindow = MyWindow()

    # window = QtWidgets.QMainWindow()
    # render_widget = vtkQtFrame.VtkQtFrame()
    # # reset the camera and set anti-aliasing to 2x
    # render_widget.VTKRenderer.ResetCamera()
    # # render_widget.VTKRenderWindow.SetAAFrames(2)
    #
    # # add and show
    # window.setCentralWidget(render_widget)
    #
    #
    # label = QtWidgets.QLabel(window)
    # label.setText("First test !")
    # label.move(50, 50)
    # button = QtWidgets.QPushButton(window)
    # button.setText("Colorer par parametre")
    # button.clicked.connect(colorByParameter)
    #
    # window.show()

    myWindow.show()

    # start the event loop
    try:
        sys.exit(app.exec_())  # clean exit
    except SystemExit as e:
        if e.code != 0:
            raise ()
        os._exit(0)


if __name__ == '__main__':
    main()
