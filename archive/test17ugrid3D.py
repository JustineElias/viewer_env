"""
Unstructured grid with line so the mesh is visible.
"""
import os
import vtk


def make_ugrid_from_csv_file(filepath="data/pcb_sgs_withtopo.csv"):
    points = vtk.vtkPoints()
    len_points = 0
    firstline = True
    filename = os.path.normcase(filepath)
    f = open(filename)
    ugrid = vtk.vtkUnstructuredGrid()
    x_value = None
    len_yz = 0
    y_value = None
    len_z = 0
    lines = []
    id = 0
    stop = False

    for line in f:

        if stop:
            # print(25)
            break
        if firstline:
            # skip first line, presumed to be a header
            firstline = False
            for pos, word in enumerate(line.split(";")):
                # Works if no z!!!
                if pos > 2:
                    new_array = vtk.vtkDoubleArray()
                    new_array.SetName(word)
                    ugrid.GetPointData().AddArray(new_array)
                    # array.SetNumberOfComponents(1)
        else:
            for pos, word in enumerate(line.split(";")):
                # print(33, word)
                # word.replace(",", ".")
                try:
                    float(word.replace(",", "."))
                except ValueError:
                    if pos == 0:
                        stop = True
                        break

                if pos == 0:
                    # print(word, word.replace(",", "."))
                    x = float(word.replace(",", "."))
                    if x_value is None or x_value == x:
                        len_yz += 1
                        x_value = x
                    else:
                        x_value = False
                if pos == 1:
                    y = float(word.replace(",", "."))
                    if y_value is None or y_value == y and x_value == x:
                        len_z += 1
                        # print(id - 1, y_value, y, word)
                        y_value = y
                if pos == 2:
                    z = float(word.replace(",", "."))
                if pos > 2:
                    array = ugrid.GetPointData().GetArray(pos - 3)
                    try:
                        array.InsertNextValue(float(word))
                    except ValueError:
                        array.InsertNextValue(1e-20)
            if not stop:
                points.InsertNextPoint(x, y, z)
                len_points += 1
            # print(id, len_points, x, y, z)
        # print(id)
        id += 1

    len_y = int(len_yz / len_z)
    len_x = int(len_points / len_yz)
    print(49, "len_y", len_y, "\nlen_x", len_x, "\nlen_z", len_z, "\nlen_yz", len_yz, "\nlen_points", len_points)

    for i in range(len_points):
        print(83, i)
        ugrid.InsertNextCell(vtk.VTK_VERTEX, 1, [i])
        print(85)
        if i // len_z == (i + 1) // len_z:
            print(87, i)
            ugrid.InsertNextCell(vtk.VTK_LINE, 2, [i, i + 1])
            print(89, i)
        if i // len_yz == (i + len_z) // len_yz:
            ugrid.InsertNextCell(vtk.VTK_LINE, 2, [i, i + len_z])
        if i + len_yz < len_points:
            ugrid.InsertNextCell(vtk.VTK_LINE, 2, [i, i + len_yz])
    print(84)
    ugrid.SetPoints(points)

    return ugrid


if __name__ == "__main__":
    make_ugrid_from_csv_file()
