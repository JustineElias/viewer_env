from PyQt5 import QtWidgets
import vtk
import sys
import os
import vtkQtFrame
from archive.test14ugrid import make_ugrid_from_csv_file




class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.initUI()

    def initUI(self):
        self.render_widget_ = vtkQtFrame.VtkQtFrame()

        self.ugrid = make_ugrid_from_csv_file()
        self.ugridMapper = vtk.vtkDataSetMapper()
        self.ugridMapper.SetInputData(self.ugrid)

        self.ugrid_actor = vtk.vtkActor()
        self.ugrid_actor.SetMapper(self.ugridMapper)
        self.ugrid_actor.GetProperty().SetRenderPointsAsSpheres(1)
        self.ugrid_actor.GetProperty().SetPointSize(3)

        self.render_widget_.VTKRenderer.AddActor(self.ugrid_actor)

        self.render_widget_.VTKRenderer.SetBackground(0, 0, 0)

        # reset the camera and set anti-aliasing to 2x
        self.render_widget_.VTKRenderer.ResetCamera()

        # add and show
        self.setCentralWidget(self.render_widget_)

        self.button_ = QtWidgets.QPushButton(self)
        self.button_.setText("Colorer par parametre")
        self.button_.clicked.connect(self.colorByParameter)

        self.show()

    def colorByParameter(self):
        ##do that
        print("Clicked colorByParameter")
        # Source https://lorensen.github.io/VTKExamples/site/Python/Widgets/ScalarBarWidget/

        # scalar_range = self.polydata.GetScalarRange()       # Original line
        # scalar_range = self.polydata.GetPointData().GetArray(0).GetValueRange()
        array = self.ugrid.GetPointData().GetArray(2)
        scalar_range = array.GetValueRange()
        lut = vtk.vtkLookupTable()
        lut.SetRange(scalar_range)
        lut.SetNumberOfTableValues(255)
        for i in range(255):
            lut.SetTableValue(i, i / 255.0, 0.0, 1 - i / 255.0, 1.0)
        lut.Build()

        self.ugrid.GetPointData().SetScalars(array)

        self.ugridMapper.UseLookupTableScalarRangeOff()
        self.ugridMapper.SetLookupTable(lut)
        self.ugridMapper.SetScalarRange(scalar_range)

        # self.ugridMapper.SetLookupTable(lut)


        # # Doen't work
        # self.actor_scalar_bar = vtk.vtkActor()
        # self.actor_scalar_bar.SetMapper(self.mapper_scalar_bar)
        #
        # self.render_widget_.VTKRenderer.AddActor(self.actor_scalar_bar)

        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToHorizontal()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget_.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()

        self.render_widget_.VTKRenderWindowInteractor.Initialize()
        self.render_widget_.VTKRenderWindowInteractor.Start()
        self.render_widget_.VTKRenderWindowInteractor.ReInitialize()
        self.show()

        # self.polydata.SetLookUpTable().

    # def make_buttons_


def main():
    app = QtWidgets.QApplication(sys.argv)
    myWindow = MyWindow()

    # window = QtWidgets.QMainWindow()
    # render_widget = vtkQtFrame.VtkQtFrame()
    # # reset the camera and set anti-aliasing to 2x
    # render_widget.VTKRenderer.ResetCamera()
    # # render_widget.VTKRenderWindow.SetAAFrames(2)
    #
    # # add and show
    # window.setCentralWidget(render_widget)
    #
    #
    # label = QtWidgets.QLabel(window)
    # label.setText("First test !")
    # label.move(50, 50)
    # button = QtWidgets.QPushButton(window)
    # button.setText("Colorer par parametre")
    # button.clicked.connect(colorByParameter)
    #
    # window.show()

    myWindow.show()

    # start the event loop
    try:
        sys.exit(app.exec_())  # clean exit
    except SystemExit as e:
        if e.code != 0:
            raise ()
        os._exit(0)


if __name__ == '__main__':
    main()