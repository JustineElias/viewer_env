##http://www.ifnamemain.com/posts/2013/Dec/08/python_qt_vtk/


from PyQt5 import QtWidgets
import vtk
import sys
import os
import vtkQtFrame
from archive.cvs_reader_from_web import polyDataOutput


class MyWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MyWindow, self).__init__()
        self.initUI()

    def initUI(self):
        self.render_widget_ = vtkQtFrame.VtkQtFrame()

        # Create source
        # source = vtk.vtkSphereSource()
        # source.SetCenter(0, 0, 0)
        # source.SetRadius(5.0)

        # # Create polydata from csv file
        # polyData = polyDataOutput
        #
        # # Create a mapper
        # mapper = vtk.vtkPolyDataMapper()
        # mapper.SetInputData(polyData)

        # Create polydata from csv file
        self.polydata = polyDataOutput
        print(14, self.polydata.GetPointData().GetArray(0))
        # array = self.polydata.GetPointData().GetArray(2)
        # help(array)

        point_data = self.polydata.GetPointData().GetArray(2)
        # self.polydata.GetPointData().SetActiveScalars(point_data.GetName())
        # self.polydata.GetPointData().SetAttribute(point_data, 0)

        # Create anything you want here, we will use a cube for the demo.
        self.cubeSource = vtk.vtkCubeSource()
        size = 5
        self.cubeSource.SetXLength(size)
        self.cubeSource.SetYLength(size)
        self.cubeSource.SetZLength(size)
        # This allows to put cubes centered on the points
        self.glyph3D = vtk.vtkGlyph3D()
        self.glyph3D.SetSourceConnection(self.cubeSource.GetOutputPort())
        self.glyph3D.SetInputData(self.polydata)
        # self.glyph3D.SetColorModeToColorByScalar()
        self.glyph3D.Update()
        # # Create a filter that colors the glyph with an array
        # self.glyph_filter = vtk.vtkGenericGlyph3DFilter()
        # self.glyph_filter.SetInputConnection(self.glyph3D.GetOutputPort())
        # self.glyph_filter.SetSourceData(self.polydata)
        # self.glyph_filter.SetColorModeToColorByScalar()

        # Visualize
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInputConnection(self.glyph3D.GetOutputPort())

        # Create an actor
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)

        self.render_widget_.VTKRenderer.AddActor(self.actor)

        self.render_widget_.VTKRenderer.SetBackground(0, 0, 0)

        # for i in range(0, 10):
        #     # random 3D position between 0,10
        #     r = numpy.random.rand(3) * 10.0
        #     # random RGB color between 0,1
        #     c = numpy.random.rand(3)
        #     # create new sphere actor
        #     my_sphere = sphere_actor.SphereActor(1.0, 20, r, c)
        #     # add to renderer
        #     self.render_widget_.VTKRenderer.AddActor(my_sphere)
        #     print(i)

        # reset the camera and set anti-aliasing to 2x
        self.render_widget_.VTKRenderer.ResetCamera()
        # self/render_widget_.VTKRenderWindow.SetAAFrames(2)

        # add and show
        self.setCentralWidget(self.render_widget_)

        # self.label_ = QtWidgets.QLabel(self)
        # self.label_.setText("First test !")
        # self.label_.move(50, 50)
        self.button_ = QtWidgets.QPushButton(self)
        self.button_.setText("Colorer par parametre")
        self.button_.clicked.connect(self.colorByParameter)

        self.show()

    def colorByParameter(self):
        ##do that
        print("Clicked colorByParameter")
        # Source https://lorensen.github.io/VTKExamples/site/Python/Widgets/ScalarBarWidget/

        # scalar_range = self.polydata.GetScalarRange()       # Original line
        array = self.polydata.GetPointData().GetArray(0)
        scalar_range = array.GetValueRange()
        lut = vtk.vtkLookupTable()
        lut.SetRange(scalar_range)
        lut.SetNumberOfTableValues(255)
        for i in range(255):
            lut.SetTableValue(i, i / 255.0, 0.0, 1 - i / 255.0, 1.0)
        lut.Build()
        # array = self.polydata.GetPointData().GetArray(0)
        # print(99)
        # self.polydata.GetPointData().SetAttribute(array, 0)
        # print(100)
        # print(101, type(self.polydata.GetPointData().GetScalars()))

        self.polydata.GetPointData().SetScalars(array)
        # self.glyph3D.SetColorModeToColorByScalar()
        self.glyph3D.Update()



        print(112, scalar_range)
        # self.mapper.SetScalarRange(scalar_range)
        # self.mapper.SetLookupTable(lut)
        # self.mapper.SetUseLookupTableScalarRange(True)
        # self.mapper.SetScalarModeToUsePointData()
        # self.mapper.SetScalarVisibility(True)
        # self.mapper.Update()


        # # Doen't work
        # self.actor_scalar_bar = vtk.vtkActor()
        # self.actor_scalar_bar.SetMapper(self.mapper_scalar_bar)
        #
        # self.render_widget_.VTKRenderer.AddActor(self.actor_scalar_bar)

        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToHorizontal()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget_.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()

        self.render_widget_.VTKRenderWindowInteractor.Initialize()
        self.render_widget_.VTKRenderWindowInteractor.Start()
        self.render_widget_.VTKRenderWindowInteractor.ReInitialize()
        self.show()

        # self.polydata.SetLookUpTable().


def main():
    app = QtWidgets.QApplication(sys.argv)
    myWindow = MyWindow()

    # window = QtWidgets.QMainWindow()
    # render_widget = vtkQtFrame.VtkQtFrame()
    # # reset the camera and set anti-aliasing to 2x
    # render_widget.VTKRenderer.ResetCamera()
    # # render_widget.VTKRenderWindow.SetAAFrames(2)
    #
    # # add and show
    # window.setCentralWidget(render_widget)
    #
    #
    # label = QtWidgets.QLabel(window)
    # label.setText("First test !")
    # label.move(50, 50)
    # button = QtWidgets.QPushButton(window)
    # button.setText("Colorer par parametre")
    # button.clicked.connect(colorByParameter)
    #
    # window.show()

    myWindow.show()

    # start the event loop
    try:
        sys.exit(app.exec_())  # clean exit
    except SystemExit as e:
        if e.code != 0:
            raise ()
        os._exit(0)


if __name__ == '__main__':
    main()

# def main():
#     app = QtWidgets.QApplication(sys.argv)
#
#     # create our new Qt MainWindow
#     window = QtWidgets.QMainWindow()
#
#     # create our new custom VTK Qt widget
#     render_widget = vtkQtFrame.VtkQtFrame()
#
#     # for i in range(0, 10):
#     #     # random 3D position between 0,10
#     #     r = numpy.random.rand(3) * 10.0
#     #     # random RGB color between 0,1
#     #     c = numpy.random.rand(3)
#     #     # create new sphere actor
#     #     my_sphere = sphere_actor.SphereActor(1.0, 20, r, c)
#     #     # add to renderer
#     #     render_widget.VTKRenderer.AddActor(my_sphere)
#
#     # reset the camera and set anti-aliasing to 2x
#     render_widget.VTKRenderer.ResetCamera()
#     # render_widget.VTKRenderWindow.SetAAFrames(2)
#
#     # add and show
#     window.setCentralWidget(render_widget)
#     window.show()
#
#     # start the event loop
#     try:
#         sys.exit(app.exec_())
#     except SystemExit as e:
#         if e.code != 0:
#             raise ()
#         os._exit(0)
#
#
# if __name__ == '__main__':
#     main()
