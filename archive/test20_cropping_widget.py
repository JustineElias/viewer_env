from PyQt5 import QtWidgets
import vtk
import sys
import os
import vtkQtFrame
from archive.test18imagedata import make_image_from_csv_file


# Call back function to resize the cone
class MyWindow:
    def __init__(self):
        self.main()

    def main(self):
        app = QtWidgets.QApplication(sys.argv)
        self.render_widget = vtkQtFrame.VtkQtFrame()

        colors = vtk.vtkNamedColors()

        # # Main pipeline
        # Create a vtkImageData
        self.img = make_image_from_csv_file()

        self.img_mapper = vtk.vtkDataSetMapper()
        self.img_mapper.SetInputData(self.img)
        self.img_actor = vtk.vtkActor()
        self.img_actor.SetMapper(self.img_mapper)
        self.render_widget.VTKRenderer.AddActor(self.img_actor)
        print(29, self.img_actor.GetProperty().GetRepresentation())
        self.img_actor.GetProperty().EdgeVisibilityOn()

        # # Display in custom vtk Qt Window
        self.window = QtWidgets.QMainWindow()
        self.window.setCentralWidget(self.render_widget)

        # Buttons
        self.clipper = None
        self.b_color_by = QtWidgets.QPushButton(self.window)
        self.b_color_by.setText("Colorer par parametre")
        self.b_color_by.clicked.connect(self.colorByParameter)

        self.image_cropping_regions_widget = None

        self.image_cropping_regions_widget = vtk.vtkImageCroppingRegionsWidget()
        print(116)
        self.image_cropping_regions_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
        print(118)
        self.image_cropping_regions_widget.SetProp3D(self.img_actor)
        print(120)
        self.image_cropping_regions_widget.SetPlaceFactor(1.25)  # Make the box 1.25x larger than the actor
        print(122)
        # self.image_cropping_regions_widget.PlaceWidget(0, 0, 0, 0, 0, 0)
        print(124)

        self.b_add_clipper = QtWidgets.QCheckBox(self.window)
        self.b_add_clipper.setStyleSheet("color: white")
        self.b_add_clipper.setText("Ajouter un plan de coupe")
        self.b_add_clipper.stateChanged.connect(self.add_clipping_plane)
        self.b_add_clipper.move(0, 50)

        self.render_widget.VTKRenderer.SetBackground(0, 0, 0)
        self.render_widget.VTKRenderer.ResetCamera()

        self.window.show()
        # start the event loop
        try:
            sys.exit(app.exec_())  # clean exit
        except SystemExit as e:
            if e.code != 0:
                raise ()
            os._exit(0)

    def planeCallback(self, obj, event):
        self.plane.SetOrigin(self.image_cropping_regions_widget.GetOrigin())
        self.plane.SetNormal(self.image_cropping_regions_widget.GetNormal())

    def colorByParameter(self):

        array = self.img.GetCellData().GetArray(1)
        self.img.GetCellData().SetScalars(array)

        scalar_range = array.GetRange()
        name = array.GetName()

        if self.clipper is not None:
            data_color_mapper = vtk.vtkDataSetMapper()
            data_color_mapper.SetInputConnection(self.clipper.GetOutputPort())
        else:
            data_color_mapper = self.img_mapper
        data_color_mapper.SelectColorArray(name)
        data_color_mapper.SetScalarRange(scalar_range)

        # Create the lookup table
        data_color_mapper.CreateDefaultLookupTable()
        lut = data_color_mapper.GetLookupTable()
        data_color_mapper.GetLookupTable().UseBelowRangeColorOn()
        data_color_mapper.GetLookupTable().SetTableRange(0.01, 1)
        # data_color_mapper.GetLookupTable().SetNanColor(0.2, 0.2, 0.2, 0.5)
        print(lut)

        # Create the actor
        if self.clipper is not None:
            data_color_actor = vtk.vtkActor()
            data_color_actor.SetMapper(data_color_mapper)
            self.render_widget.VTKRenderer.AddActor(data_color_actor)
        else:
            self.img_actor.SetMapper(data_color_mapper)

        # create the scalar_bar TODO: adapt size and position (it is naturally pickable)
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetOrientationToHorizontal()
        self.scalar_bar.SetLookupTable(lut)

        # create the scalar_bar_widget
        self.scalar_bar_widget = vtk.vtkScalarBarWidget()
        self.scalar_bar_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
        self.scalar_bar_widget.SetScalarBarActor(self.scalar_bar)
        self.scalar_bar_widget.On()

        self.render_widget.VTKRenderWindowInteractor.Initialize()

    def add_clipping_plane(self):
        if self.b_add_clipper.isChecked() and self.img_actor.GetVisibility():
            # if self.image_cropping_regions_widget is None:
            if True:
                # A plane widget
                self.image_cropping_regions_widget = vtk.vtkImageCroppingRegionsWidget()
                print(116)
                self.image_cropping_regions_widget.SetInteractor(self.render_widget.VTKRenderWindowInteractor)
                print(118)
                self.image_cropping_regions_widget.SetProp3D(self.img_actor)
                print(120)
                self.image_cropping_regions_widget.SetPlaceFactor(1.25)  # Make the box 1.25x larger than the actor
                print(122)
                self.image_cropping_regions_widget.PlaceWidget()
                print(136)
                # Create plane from plane_widget
                self.plane = vtk.vtkPlane()
                self.plane.SetOrigin(self.image_cropping_regions_widget.GetOrigin())
                self.plane.SetNormal(self.image_cropping_regions_widget.GetNormal())

                # Create the clipped version of the data
                self.clipper = vtk.vtkClipDataSet()
                self.clipper.SetInputData(self.img)
                self.clipper.SetClipFunction(self.plane)
                self.clipper_mapper = vtk.vtkDataSetMapper()
                self.clipper_mapper.SetInputConnection(self.clipper.GetOutputPort())
                # imgdataMapper.SetInputData(self.imageData)
                self.clipper_actor = vtk.vtkActor()
                self.clipper_actor.SetMapper(self.clipper_mapper)
                # self.clipper_actor.GetProperty().EdgeVisibilityOn()
                # self.clipper_actor.GetProperty().SetColor(colors.GetColor3d("BurlyWood"))
                # self.clipper_actor.GetProperty().SetRepresentationToWireframe()
                self.render_widget.VTKRenderer.AddActor(self.clipper_actor)

                # Connect the event to a function
                self.image_cropping_regions_widget.AddObserver("InteractionEvent", self.planeCallback)

            # Hide the original img actor and show the clipper actor
            self.img_actor.VisibilityOff()
            self.image_cropping_regions_widget.On()
            self.clipper_actor.VisibilityOn()
        else:
            self.img_actor.VisibilityOn()
            self.image_cropping_regions_widget.Off()
            self.clipper_actor.VisibilityOff()

        # Following line updates the renderer so that all the action above are displayed
        self.render_widget.VTKRenderWindowInteractor.Initialize()


# interactor.Start()
if __name__ == '__main__':
    w = MyWindow()
