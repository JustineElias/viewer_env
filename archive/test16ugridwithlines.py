"""
Unstructured grid with line so the mesh is visible.
"""
import os
import vtk


def make_ugrid_from_csv_file(filepath="data/exemple_fichier_import_2D.csv"):
    points = vtk.vtkPoints()
    len_points = 0
    firstline = True
    filename = os.path.normcase(filepath)
    f = open(filename)
    ugrid = vtk.vtkUnstructuredGrid()
    x_value = None
    len_y = 0
    lines = []
    id = 0

    for line in f:
        if firstline:
            # skip first line, presumed to be a header
            firstline = False
            for pos, word in enumerate(line.split(";")):
                # Works if no z!!!
                if pos >= 2:
                    new_array = vtk.vtkDoubleArray()
                    new_array.SetName(word)
                    ugrid.GetPointData().AddArray(new_array)
                    # array.SetNumberOfComponents(1)
        else:
            for pos, word in enumerate(line.split(";")):
                # print(33, word)
                if pos == 0:
                    x = float(word)
                    if x_value is None or x_value == x:
                        len_y += 1
                        x_value = x
                if pos == 1:
                    y = float(word)
                # if pos == 2:
                #     z = float(word)
                if pos >= 2:
                    array = ugrid.GetPointData().GetArray(pos - 2)
                    array.InsertNextValue(float(word))
            points.InsertNextPoint(x, y, 0)
            len_points += 1

    print(49, "len_y", len_y, len_points)

    for i in range(len_points):
        ugrid.InsertNextCell(vtk.VTK_VERTEX, 1, [i])
        if i // len_y == (i + 1) // len_y:
            ugrid.InsertNextCell(vtk.VTK_LINE, 2, [i, i + 1])
        if i + len_y < len_points:
            ugrid.InsertNextCell(vtk.VTK_LINE, 2, [i, i + len_y])

    ugrid.SetPoints(points)

    return ugrid


if __name__ == "__main__":
    make_ugrid_from_csv_file()
