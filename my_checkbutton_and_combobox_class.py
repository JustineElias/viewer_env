from PyQt5 import QtWidgets, QtCore
import vtk
import sys
import os
import vtkQtFrame


class MyCheckbutton(QtWidgets.QCheckBox):
    def __init__(self, parent_window, function, items, text):
        super(MyCheckbutton, self).__init__(parent=parent_window)
        self.function = function
        self.items = items
        self.stateChanged.connect(self.add_or_hide_combobox)
        self.list_child_widget = []
        self.combobox = None
        self.setStyleSheet("color: white")
        self.setText(text)

    def add_or_hide_combobox(self):
        print(19, self.isChecked())
        if self.isChecked() and self.combobox is None:
            print(21)
            self.combobox = QtWidgets.QComboBox(self.parent())
            for item in self.items:
                self.combobox.addItem(item)
            self.combobox.currentTextChanged.connect(self.function)
            self.combobox.move(0, 100)
            self.combobox.show()
            print(28)
        elif self.isChecked() and self.combobox is not None:
            print(29)
            self.combobox.show()
        else:
            print(32)
            self.combobox.hide()
            for w in self.list_child_widget:
                print(37, w[1])
                if w[1] == "vtk":
                    print(39)
                    w[0].Off()
                else:
                    w[0].hide()


    def add_child_widget(self, widget, library):
        print(46)
        self.list_child_widget.append([widget, library])

    def get_combobox_text(self):
        return self.combobox.currentText()

    def get_combobox_index(self):
        return self.combobox.currentIndex()
